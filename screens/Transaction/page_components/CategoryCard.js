import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Constant from '../../../utilities/Constant';
import StringSanitizer from '../../../utilities/StringSanitizer';

export default function CategoryCard({ data }) {
  return (
    <View style={localStyles.container}>
      <View style={[localStyles.colorContainer, { backgroundColor: data.color }]}>
        <Text style={localStyles.colorLabel}>{data.text}</Text>
      </View>
      <View style={localStyles.amountContainer}>
        <Text style={localStyles.thinLabel}>Budget yg dialokasikan</Text>
        <Text style={localStyles.nominalLabel}>Rp {StringSanitizer.sanitizeText(data.limit)}</Text>
        <Text style={localStyles.thinLabel}>Budget yang terpakai</Text>
        <Text style={[localStyles.nominalLabel, { color: Constant.Color.SUCCESS }]}>Rp {StringSanitizer.sanitizeText(data.spent)}</Text>
      </View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
    overflow: 'hidden',
    marginBottom: 16
  },
  colorContainer: {
    width: "100%",
    padding: 16,
    alignItems: 'center',
  },
  colorLabel: {
    color: Constant.Color.ABSOLUTE_WHITE,
    fontWeight: 'bold'
  },
  amountContainer: {
    padding: 16,
    flex: 1
  },
  nominalLabel: {
    color: Constant.Color.DARK_LABEL,
    marginBottom: 8,
    fontSize: 24,
    fontWeight: 'bold'
  },
  thinLabel: {
    color: Constant.Color.THIN_LABEL,
    marginBottom: 4
  }
})