import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import StaticHeader from '../../components/header/StaticHeader';
import CategoryCard from './page_components/CategoryCard';
import Constant from '../../utilities/Constant';

// import custom data
import historyTransactionData from '../../data/historyTransactionData.json';
import HistoryTransaction from './HistoryTransaction';
import { useNavigation } from '@react-navigation/native';

const BudgetingTransaction = (props) => {
  // begin state declaration
  const [transactionData] = useState(historyTransactionData.data);
  const navigation = useNavigation()
  // end of state declaration

  function handleRedirectUser() {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={localStyle.container}>
      <StaticHeader
        title="Detail Kategori"
        type="right-action-button"
        onBackPress={() => handleRedirectUser()}
      />
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={localStyle.contentContainer}
        ListHeaderComponent={
          <>
            <View style={{
              backgroundColor: Constant.Color.THIN_GRAY_BACKGROUND,
              padding: 16,
            }}>
              <CategoryCard
                data={props.route.params.data}
              />
            </View>
            <View style={localStyle.historyTransactionContainer}>
              <View style={localStyle.actionContainer}>
                <View style={localStyle.textInputContainer}>
                  <TextInput
                    placeholder='Search'
                    style={localStyle.textInput}
                  />
                  <Icon
                    name='ios-search-outline'
                    size={16}
                  />
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity style={localStyle.actionButton}>
                    <Image
                      source={require('../../assets/icons/list_box.png')}
                      style={{ width: 16, height: 16 }}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity style={localStyle.actionButton}>
                    <Image
                      source={require('../../assets/icons/filter.png')}
                      style={{ width: 16, height: 16 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <HistoryTransaction
              data={transactionData}
            />
          </>
        }
      />
    </SafeAreaView>
  );
}

export default BudgetingTransaction;

const localStyle = StyleSheet.create({
  container: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    flex: 1
  },
  contentContainer: {
    paddingBottom: 0,
    backgroundColor: Constant.Color.THIN_GRAY_BACKGROUND
  },
  historyTransactionContainer: {
    flex: 1,
    padding: 16,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
  },
  textInputContainer: {
    flex: 1,
    height: 48,
    flexDirection: 'row',
    marginRight: 8,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderRadius: 8,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Constant.Color.THIN_GRAY_BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 8
  },
  textInput: {
    flex: 1,
    height: 48,
    borderColor: 'red',
    backgroundColor: Constant.Color.ABSOLUTE_WHITE
  },
  actionContainer: {
    flexDirection: 'row'
  },
  actionButton: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    padding: 15,
    borderRadius: 8,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Constant.Color.THIN_GRAY_BACKGROUND
  }
})