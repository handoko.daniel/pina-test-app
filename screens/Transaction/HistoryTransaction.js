import React, { } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Constant from '../../utilities/Constant';
import StringSanitizer from '../../utilities/StringSanitizer';

export default function HistoryTransaction(props) {
  return (
    <View style={localStyles.container}>
      {
        props.data.map((item, index) =>
          <View
            key={index}
            style={localStyles.transactionContainer}
          >
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={localStyles.title}>{item.name}</Text>
              </View>
              <View style={{ flex: 1, alignItems: 'flex-end', }}>
                <Text style={localStyles.amountLabel}>Rp {StringSanitizer.sanitizeText(item.amount)}</Text>
              </View>
            </View>
            <Text style={localStyles.thinLabel}>{item.source}</Text>
          </View>
        )
      }
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
  },
  transactionContainer: {
    borderBottomWidth: 1,
    borderColor: Constant.Color.THIN_GRAY,
    padding: 16,
  },
  title: {
    color: Constant.Color.DARK_LABEL,
    fontWeight: 'bold',
    marginBottom: 8
  },
  thinLabel: {
    color: Constant.Color.THIN_LABEL,
  },
  amountLabel: {
    color: Constant.Color.PRIMARY,
    fontWeight: 'bold'
  }
})