import React, { useState } from 'react';
import { View, Text, StatusBar, FlatList, StyleSheet, Image, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import moment from "moment";
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import PrimaryButton from '../../components/button/PrimaryButton';
import LabeledDropDown from '../../components/forms/LabeledDropDown';
import LabeledTextInput from '../../components/forms/LabeledTextInput';
import StaticHeader from '../../components/header/StaticHeader';
import TransactionSwitcher from '../../components/utilities/TransactionSwitcher';
import Constant from '../../utilities/Constant';
import SlideUpSelectionModal from '../../components/modal/SlideUpSelectionModal';
import LabeledDatePicker from '../../components/forms/LabeledDatePicker';
import LabeledCurrency from '../../components/forms/LabeledCurrency';
import CustomProgressBar from '../../components/utilities/CustomProgressBar';
import { useNavigation } from '@react-navigation/native';

// import dummy data
import expenseData from '../../data/expenseData.json';

export default function CreateExpense() {
  // start state declaration
  const [name, setName] = useState('');
  const [category, setCategory] = useState('');
  const [expenseSource, setExpenseSource] = useState('');
  const [totalExpense, setTotalExpense] = useState('0');
  const [startDate, setStartDate] = useState('Pilih Tanggal');

  // utilities state
  const navigation = useNavigation();
  const [modalStatus, setModalStatus] = useState(false);
  const [dataType, setDataType] = useState("default");
  const [categoryData] = useState(expenseData.data);
  const [expenseSourceData] = useState([
    {
      "icon": require('../../assets/illustrations/cash.png'),
      "text": "Cash"
    },
    {
      "icon": require('../../assets/illustrations/bank.png'),
      "text": "Bank",
      "bank_list": [
        {
          "icon": require('../../assets/illustrations/bca.png'),
          "name": "BCA",
          "no_rek": "3848"
        },
        {
          "icon": require('../../assets/illustrations/bni.png'),
          "name": "BNI",
          "no_rek": "2565"
        }
      ]
    },
    {
      "icon": require('../../assets/illustrations/cash.png'),
      "text": "E-Wallet"
    },
    {
      "icon": require('../../assets/illustrations/credit_card.png'),
      "text": "Credit Card"
    },
  ])
  // end of state declaration

  const handleChangeText = (id, value) => {
    if (id == "name") {
      setName(value);
    }
    else if (id == "total_expense") {
      setTotalExpense(value);
    }
    else if (id == "total_expense_increment") {
      if (value != "") {
        let expense = parseInt(value);
        expense += 1;
        setTotalExpense(expense.toString());
      }
      else {
        if (totalExpense != "") {
          let expense = parseInt(totalExpense);
          expense += 1;
          setTotalExpense(expense.toString());
        }
        else {
          let expense = 0;
          expense += 1;
          setTotalExpense(expense.toString());
        }
      }
    }
    else if (id == "total_expense_decrement") {
      if (value != "") {
        let expense = parseInt(value);
        if (expense > 0) {
          expense -= 1;
        }
        else {
          expense = 0;
        }
        setTotalExpense(expense.toString());
      }
      else {
        if (totalExpense != "") {
          let expense = parseInt(totalExpense);
          expense -= 1;
          setTotalExpense(expense.toString());
        }
        else {
          let expense = 0;
          expense -= 1;
          setTotalExpense(expense.toString());
        }
      }
    }
  }

  const handleSelectCategory = (value) => {
    if (dataType == "category") {
      setCategory(value);
    }
    else if (dataType == "expense") {
      setExpenseSource(value);
    }
    setModalStatus(false)
  }

  function handleDataSource() {
    if (dataType == "category") {
      return categoryData;
    }
    else if (dataType == "expense") {
      return expenseSourceData;
    }
  }

  function handleChangeDataLabel() {
    if (dataType == "category") {
      return "Pilih Kategori";
    }
    else if (dataType == "expense") {
      return "Pilih Sumber Pengeluaran";
    }
  }

  const handleDateChange = (id, value) => {
    if (value.type != "dismissed") {
      var date = moment(new Date()).format('MM/DD/YYYY hh:MM').split(' ');

      date = moment(value).format('MM/DD/YYYY hh:MM').split(' ');
      let formatted_date = date[0].split('/');

      if (id == "start_date") {
        setStartDate(formatted_date[1] + "-" + formatted_date[0] + "-" + formatted_date[2]);
      }
    }
  }

  function handleRedirectUser() {
    navigation.navigate('BudgetingTransaction', { data: category });
  }

  function renderModal() {
    return (
      <Modal
        isVisible={modalStatus}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        animationInTiming={500}
        animationOutTiming={500}
        onBackdropPress={() => setModalStatus(false)}
        style={{
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
      >
        {renderModalType()}
      </Modal >
    )
  }

  function renderModalType() {
    return (
      <SlideUpSelectionModal
        id={dataType}
        data={handleDataSource()}
        title={handleChangeDataLabel()}
        onSelectionPressed={handleSelectCategory}
      />
    )
  }

  function renderProgressBar() {
    if (category != '') {
      let percentage = category.spent / category.limit;
      return (
        <CustomProgressBar
          data={category}
          percentage={percentage}
          onPress={() => handleRedirectUser()}
        />
      )
    }
  }

  return (
    <SafeAreaView style={localStyle.container}>
      <KeyboardAvoidingView behavior='height' enabled>
        <StatusBar
          backgroundColor={Constant.Color.ABSOLUTE_WHITE}
          barStyle='dark-content'
        />
        <StaticHeader
          title="Buat Transaksi"
          type="back-button"
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={localStyle.contentContainer}
          ListHeaderComponent={
            <>
              <TransactionSwitcher
                expenseTitle="Pengeluaran"
                suspenseTitle="Pemasukan"
              />
              <Image
                source={require('../../assets/illustrations/wallet.png')}
                style={localStyle.icon}
              />
              <LabeledDropDown
                id="category"
                label="Pilih Kategori"
                placeholder="Pilih Kategori"
                value={category}
                onPress={() => {
                  setDataType("category");
                  setModalStatus(true);
                }}
              />
              <LabeledTextInput
                id="name"
                label="Merchant"
                placeholder="Merchant"
                value={name}
                onChangeText={handleChangeText}
                type="active"
              />
              <LabeledDropDown
                id="expense"
                label="Sumber Pengeluaran"
                placeholder="Sumber Pengeluaran"
                value={expenseSource}
                onPress={() => {
                  setDataType("expense");
                  setModalStatus(true);
                }}
              />
              <LabeledCurrency
                id="total_expense"
                label="Jumlah Pengeluaran"
                placeholder="Jumlah Pengeluaran"
                value={totalExpense}
                keyboardType="number-pad"
                onChangeText={handleChangeText}
                type="active"
              />
              <LabeledDatePicker
                id="start_date"
                label="Promo Dimulai"
                value={startDate}
                onDateChange={handleDateChange}
              />
              {renderProgressBar()}
              <View style={localStyle.actionContainer}>
                <View>
                  <TouchableOpacity style={localStyle.cameraButton}>
                    <Icon
                      name='ios-camera-outline'
                      size={16}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                  <PrimaryButton
                    label="Tambah"
                    isActive
                  />
                </View>
              </View>
            </>
          }
        />
        {renderModal()}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const localStyle = StyleSheet.create({
  container: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    flex: 1
  },
  contentContainer: {
    padding: 16,
    paddingTop: 0
  },
  icon: {
    width: Constant.Measure.SCREEN_WIDTH * 0.2,
    height: Constant.Measure.SCREEN_WIDTH * 0.2,
    resizeMode: "contain",
    marginBottom: 8
  },
  actionContainer: {
    flexDirection: 'row'
  },
  cameraButton: {
    backgroundColor: Constant.Color.SECONDARY_PRIMARY,
    padding: 15,
    borderRadius: 8,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center',
  }
})