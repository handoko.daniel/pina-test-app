// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import CreateExpense from '../screens/Expense/CreateExpense';
import BudgetingTransaction from '../screens/Transaction/BudgetingTransaction';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="CreateExpense" component={CreateExpense} options={{ headerShown: false }} />
        <Stack.Screen name="BudgetingTransaction" component={BudgetingTransaction} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;