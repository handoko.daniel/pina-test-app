import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import Constant from '../../utilities/Constant';
import StringSanitizer from '../../utilities/StringSanitizer';

export default function CustomProgressBar(props) {
  return (
    <TouchableOpacity
      style={localStyles.container}
      onPress={props.onPress}
    >
      <Text style={localStyles.title}>Sisa budget untuk {props.data.text}</Text>
      <ProgressBar progress={props.percentage} color={props.data.color} style={{ height: 32, borderRadius: 40 }} />
      <Text style={localStyles.label}>{StringSanitizer.sanitizeText(props.data.spent)} / {StringSanitizer.sanitizeText(props.data.limit)}</Text>
    </TouchableOpacity>
  )
}

const localStyles = StyleSheet.create({
  container: {
    marginTop: 8,
    marginBottom: 16
  },
  title: {
    fontSize: 14,
    marginBottom: 8,
    fontWeight: 'bold'
  },
  label: {
    color: Constant.Color.ABSOLUTE_WHITE,
    marginTop: -22,
    textAlign: 'center'
  }
})