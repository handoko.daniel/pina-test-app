import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';

export default function TransactionSwitcher(props) {
  return (
    <View style={localStyle.container}>
      <View
        style={localStyle.buttonContainer}
      >
        <TouchableOpacity style={[localStyle.transactionButton, localStyle.activeButton]}>
          <Text style={localStyle.activeLabel}>{props.expenseTitle}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={localStyle.transactionButton}>
        <Text style={localStyle.inActiveLabel}>{props.suspenseTitle}</Text>
        </TouchableOpacity>
      </View>
    </View >
  );
}

const localStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: "100%",
    paddingBottom: 16,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  transactionButton: {
    flex: 1,
    alignItems: 'center',
    padding: 8,
    marginRight: 8,
    borderRadius: 8,
    backgroundColor: Constant.Color.DARK_GRAY
  },
  activeButton: {
    backgroundColor: Constant.Color.PRIMARY
  },
  activeLabel: {
    color: Constant.Color.ABSOLUTE_WHITE,
    fontWeight: 'bold'
  },
  inActiveLabel: {
    color: Constant.Color.DARK_LABEL,
    fontWeight: 'bold'
  }
})