import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../utilities/Constant';

const StaticHeader = (props) => {
  function renderHeaderType(type) {
    if (type == "general") {
      return (
        <View style={localStyles.container}>
          <Text style={localStyles.label}>{props.title}</Text>
        </View>
      )
    }
    else if (type == "back-button") {
      return (
        <View style={localStyles.containerBackButton}>
          <Icon
            name="chevron-back"
            size={32}
            color={Constant.Color.DARK_LABEL}
            onPress={props.onBackPress}
          />
          <View style={localStyles.labelContainer}>
            <Text style={localStyles.label}>{props.title}</Text>
          </View>
        </View>
      )
    }
    else if (type == "right-action-button") {
      return (
        <View style={localStyles.containerBackButton}>
          <Icon
            name="chevron-back"
            size={32}
            color={Constant.Color.DARK_LABEL}
            onPress={props.onBackPress}
          />
          <View style={localStyles.labelContainer}>
            <Text style={localStyles.label}>{props.title}</Text>
          </View>
          <Text style={localStyles.rightActionLabel}>Atur</Text>
        </View>
      )
    }
  }

  return (
    renderHeaderType(props.type)
  );
}

const localStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: Constant.Measure.SCREEN_WIDTH,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    padding: 12,
  },
  containerBackButton: {
    width: Constant.Measure.SCREEN_WIDTH,
    padding: 12,
    flexDirection: "row",
    alignItems: 'center',
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
  },
  rightActionLabel: {
    color: Constant.Color.PRIMARY
  },
  labelContainer: {
    alignItems: 'center',
    flex: 1,
    paddingRight: 28
  },
  label: {
    color: Constant.Color.DARK_LABEL,
    fontSize: 16,
    fontWeight: 'bold'
  }
})

export default StaticHeader;