import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';
import StringSanitizer from '../../utilities/StringSanitizer';

const LabeledTextInput = (props) => {
  // begin state declaration
  const [errorMessage, setErrorMessage] = useState('');
  // end of state declaration

  function handleCheckSpelling(value, type) {
    const { required, label } = props;

    if (required && (value == null || value == "")) {
      setErrorMessage(label + " wajib diisi");
    }
    else if (type == "email-address") {
      let isValidEmail = StringSanitizer.isValidEmail(value);
      if (!isValidEmail) {
        setErrorMessage("Format " + label + " tidak sesuai");
      }
      else {
        setErrorMessage("");
      }
    }
    else {
      setErrorMessage("")
    }
  }

  function renderErrorMessage() {
    if (errorMessage != "") {
      return (
        <Text style={localStyles.errorMessage}>{errorMessage}</Text>
      )
    }
  }

  if (props.type == "active") {
    return (
      <View style={localStyles.container}>
        <Text style={localStyles.label}>{props.label}{
          props.required ?
            <Text style={[localStyles.label, { color: Constant.Color.DANGER }]}> *</Text>
            :
            <Text></Text>
        }</Text>
        <TextInput
          id={props.id}
          style={localStyles.textInput}
          placeholder={props.placeholder}
          value={props.value}
          placeholderTextColor={Constant.Color.THIN_LABEL}
          onChangeText={(value) => props.onChangeText(props.id, value)}
          secureTextEntry={props.isSecure}
          keyboardType={props.keyboardType}
          maxLength={props.maxLength}
          autoCapitalize={props.autoCapitalize}
          onEndEditing={() => handleCheckSpelling(props.value, props.keyboardType)}
        />
        {renderErrorMessage()}
      </View>
    )
  }
  else {
    return (
      <View style={localStyles.container}>
        <Text style={localStyles.label}>{props.label}</Text>
        <TextInput
          id={props.id}
          style={[localStyles.textInput, localStyles.inactiveTextinput]}
          placeholder={props.placeholder}
          value={props.value}
          placeholderTextColor={Constant.Color.THIN_LABEL}
          onChangeText={(value) => props.onChangeText(props.id, value)}
          secureTextEntry={props.isSecure}
          editable={false}
        />
      </View>
    )
  }
}

export default LabeledTextInput;

const localStyles = StyleSheet.create({
  container: {
    marginBottom: 16,
    width: "100%",
  },
  label: {
    fontSize: 14,
    marginBottom: 8,
    fontWeight: 'bold'
  },
  textInput: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderColor: Constant.Color.THIN_GRAY,
    borderRadius: 8,
    borderWidth: 1,
    fontSize: 14,
    padding: 16,
    width: "100%",
  },
  inactiveTextinput: {
    backgroundColor: Constant.Color.THIN_GRAY,
    borderColor: Constant.Color.THIN_LABEL,
    borderRadius: 8,
    borderWidth: 1,
    fontSize: 14,
    padding: 16,
    width: "100%",
  },
  errorMessage: {
    color: Constant.Color.DANGER,
    marginTop: 8
  }
})