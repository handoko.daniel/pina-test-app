import React, { useState } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Dimensions } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';

// import custom component
import Constant from '../../utilities/Constant';

export default function LabeledDatePicker({ id, label, value, onDateChange }) {
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  const handleOnDateShown = (event, selectedDate) => {
    if (Platform.OS === 'ios') {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      onDateChange(id, currentDate);
    }
    else {
      setShow(false);
      if (selectedDate != null) {
        onDateChange(id, selectedDate);
      }
    }
  }

  function handleIOSDefaultDate() {
    setShow(false);
    onDateChange(id, date);
  }

  function renderDatePicker() {
    if (Platform.OS === "android" && show) {
      return (
        <DateTimePicker
          value={date}
          mode='date'
          display='default'
          onChange={handleOnDateShown}
          minimumDate={new Date()}
          style={{
            shadowColor: '#000000',
            shadowRadius: 0,
            shadowOpacity: 1,
            shadowOffset: { height: 0, width: 0 },
          }}
        />
      )
    }
    else {
      return (
        <Modal
          isVisible={show}
          animationIn="slideInUp"
          animationOut="slideOutDown"
          animationInTiming={500}
          animationOutTiming={500}
          onBackdropPress={() => setShow(false)}
          style={{
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        >
          <View style={{ width: Dimensions.get('window').width, backgroundColor: Constant.Color.ABSOLUTE_WHITE, }}>
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              display="default"
              is24Hour={true}
              onChange={handleOnDateShown}
              minimumDate={new Date()}
              style={{ borderWidth: 1, }}
            />
            <TouchableOpacity
              style={{
                width: Dimensions.get('window').width,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 24,
                backgroundColor: Constant.Color.PRIMARY
              }}
              onPress={() => handleIOSDefaultDate()}
            >
              <Text style={[localStyles.label, { color: Constant.Color.ABSOLUTE_WHITE }]}>PILIH</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      )
    }
  }

  return (
    <View
      style={localStyles.container}
    >
      <View style={{ flex: 1 }}>
        <Text style={localStyles.title}>{label}</Text>
      </View>

      <TouchableOpacity
        onPress={() => {
          setShow(true);
        }}
      >
        <View style={localStyles.dateContainer}>
          {renderDatePicker()}
          <View style={{ flex: 1 }}>
            <Text style={localStyles.label}>{value}</Text>
          </View>
          <Icon
            name="ios-chevron-down"
            size={24}
            color={Constant.Color.THIN_LABEL}
          />
        </View>
      </TouchableOpacity>
    </View >
  );
}

const localStyles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: 'center',
    marginBottom: 8,
  },
  dateContainer: {
    flexDirection: 'row',
    flex: 1,
    borderColor: Constant.Color.DARK_GRAY,
    borderWidth: 1,
    borderRadius: 8,
    padding: 16,
    paddingVertical: 10,
    width: "100%",
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontWeight: "bold",
    marginRight: 8,
    fontSize: 14,
    marginBottom: 8
  },
  label: {
    fontSize: 14,
    marginRight: 8,
  }
})