import React, { } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../utilities/Constant';

export default function LabeledDropDown({ id, label, onPress, value, placeholder }) {
  function renderIcon() {
    if (id == "category" && value != "") {
      return (
        <View style={{ backgroundColor: value.color, padding: 8, marginRight: 8, borderRadius: 16 }} />
      )
    }
    else if (id == "expense" && value != "") {
      return (
        <Image
          source={value.icon}
          style={{ width: 32, height: 32, marginRight: 8 }}
        />
      )
    }
  }
  return (
    <View style={localStyles.container}>
      <Text style={localStyles.label}>{label}</Text>

      <TouchableOpacity
        style={localStyles.textInput}
        onPress={onPress}
      >
        {renderIcon()}
        {value == "" ? <Text style={localStyles.inactiveText}>{placeholder}</Text> : <Text style={localStyles.activeText}>{value.text}</Text>}
        <Image
          source={require('../../assets/icons/chevron_down.png')}
          style={{ width: 32, height: 32 }}
        />
      </TouchableOpacity>
    </View>
  );
}


const localStyles = StyleSheet.create({
  container: {
    marginBottom: 16,
    width: "100%",
  },
  label: {
    fontSize: 14,
    marginBottom: 8,
    fontWeight: 'bold',
  },
  textInput: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderRadius: 8,
    borderColor: Constant.Color.DARK_GRAY,
    borderWidth: 1,
    color: Constant.Color.DARK_LABEL,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    paddingVertical: 10,
    width: "100%",
  },
  inactiveText: {
    color: Constant.Color.THIN_LABEL,
    flex: 1,
  },
  activeText: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    color: Constant.Color.DARK_LABEL,
    flex: 1
  }
})