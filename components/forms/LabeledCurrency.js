import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';
import StringSanitizer from '../../utilities/StringSanitizer';

const LabeledCurrency = (props) => {
  // begin state declaration
  const [errorMessage, setErrorMessage] = useState('');
  // end of state declaration

  function handleCheckSpelling(value, type) {
    const { required, label } = props;

    if (required && (value == null || value == "")) {
      setErrorMessage(label + " wajib diisi");
    }
    else if (type == "email-address") {
      let isValidEmail = StringSanitizer.isValidEmail(value);
      if (!isValidEmail) {
        setErrorMessage("Format " + label + " tidak sesuai");
      }
      else {
        setErrorMessage("");
      }
    }
    else {
      setErrorMessage("")
    }
  }

  function renderErrorMessage() {
    if (errorMessage != "") {
      return (
        <Text style={localStyles.errorMessage}>{errorMessage}</Text>
      )
    }
  }

  if (props.type == "active") {
    return (
      <View style={localStyles.container}>
        <Text style={localStyles.label}>{props.label}{
          props.required ?
            <Text style={[localStyles.label, { color: Constant.Color.DANGER }]}> *</Text>
            :
            <Text></Text>
        }</Text>

        <View style={localStyles.currencyContainer}>
          <TouchableOpacity
            onPress={() => props.onChangeText(props.id + "_decrement", props.value)}
          >
            <Image
              source={require('../../assets/icons/minus.png')}
              style={{ width: 32, height: 32 }}
            />
          </TouchableOpacity>
          <TextInput
            id={props.id}
            style={localStyles.textInput}
            placeholder={props.placeholder}
            value={props.value}
            placeholderTextColor={Constant.Color.THIN_LABEL}
            onChangeText={(value) => props.onChangeText(props.id, value)}
            secureTextEntry={props.isSecure}
            keyboardType={props.keyboardType}
            maxLength={props.maxLength}
            autoCapitalize={props.autoCapitalize}
            onEndEditing={() => handleCheckSpelling(props.value, props.keyboardType)}
          />
          <TouchableOpacity
            onPress={() => props.onChangeText(props.id + "_increment", props.value)}
          >
            <Image
              source={require('../../assets/icons/plus.png')}
              style={{ width: 32, height: 32, marginLeft: 8 }}
            />
          </TouchableOpacity>
        </View>
        {renderErrorMessage()}
      </View>
    )
  }
  else {
    return (
      <View style={localStyles.container}>
        <Text style={localStyles.label}>{props.label}</Text>
        <View style={localStyles.currencyContainer}>
          <Image
            source={require('../../assets/icons/minus.png')}
            style={{ width: 32, height: 32, marginRight: 8 }}
          />
          <TextInput
            id={props.id}
            style={[localStyles.textInput, localStyles.inactiveTextinput]}
            placeholder={props.placeholder}
            value={props.value}
            placeholderTextColor={Constant.Color.THIN_LABEL}
            onChangeText={(value) => props.onChangeText(props.id, value)}
            secureTextEntry={props.isSecure}
            editable={false}
          />
          <Image
            source={require('../../assets/icons/plus.png')}
            style={{ width: 32, height: 32, marginLeft: 8 }}
          />
        </View>
      </View >
    )
  }
}

export default LabeledCurrency;

const localStyles = StyleSheet.create({
  container: {
    marginBottom: 16,
    width: "100%",
  },
  label: {
    fontSize: 14,
    marginBottom: 8,
    fontWeight: 'bold'
  },
  currencyContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: Constant.Color.THIN_GRAY,
    alignItems: 'center',
    paddingHorizontal: 12
  },
  textInput: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderRadius: 8,
    fontSize: 14,
    padding: 16,
    textAlign: 'center',
    flex: 1
  },
  inactiveTextinput: {
    backgroundColor: Constant.Color.THIN_GRAY,
    borderColor: Constant.Color.THIN_LABEL,
    borderRadius: 8,
    borderWidth: 1,
    fontSize: 14,
    padding: 16,
    width: "100%",
  },
  errorMessage: {
    color: Constant.Color.DANGER,
    marginTop: 8
  }
})