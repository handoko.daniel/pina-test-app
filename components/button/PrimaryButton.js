import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';

const PrimaryButton = (props) => {
  if (props.isActive) {
    return (
      <TouchableOpacity
        style={localStyles.activeContainer}
        onPress={props.onPress}
      >
        <Text style={localStyles.label}>{props.label}</Text>
      </TouchableOpacity>
    )
  }
  else {
    return (
      <View style={localStyles.inactiveContainer}>
        <Text style={localStyles.inactiveLabel}>{props.label}</Text>
      </View>
    )
  }
}

export default PrimaryButton;

const localStyles = StyleSheet.create({
  activeContainer: {
    alignItems: 'center',
    backgroundColor: Constant.Color.SECONDARY_PRIMARY,
    borderRadius: 8,
    justifyContent: 'center',
    paddingVertical: 16,
    width: "100%",
  },
  inactiveContainer: {
    alignItems: 'center',
    backgroundColor: Constant.Color.THIN_LABEL,
    borderRadius: 8,
    justifyContent: 'center',
    paddingVertical: 16,
    width: "100%",
  },

  // import from global styles
  label: {
    fontSize: 16,
    color: Constant.Color.DARK_LABEL,
    fontWeight: 'bold'
  },
  inactiveLabel: {
    fontSize: 16,
    color: Constant.Color.DARK_LABEL,
  }
})