import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';

export default function SlideUpSelectionModal({ id, title, data, onSelectionPressed }) {
  function renderItemsType(item, index) {
    if (id == "category") {
      return (
        <TouchableOpacity
          key={index}
          style={localStyles.modal}
          onPress={() => onSelectionPressed(item)}
        >
          <View style={{ backgroundColor: item.color, padding: 8, marginRight: 8, borderRadius: 16 }} />
          <Text style={{ fontSize: 14, }}>
            {item.text}
          </Text>
        </TouchableOpacity>
      )
    }
    else if (id == "expense") {
      return (
        <TouchableOpacity
          key={index}
          style={localStyles.modal}
          onPress={() => onSelectionPressed(item)}
        >
          <Image
            source={item.icon}
            style={{ width: 32, height: 32, marginRight: 8 }}
          />
          <Text style={{ fontSize: 14, }}>
            {item.text}
          </Text>
        </TouchableOpacity>
      )
    }
  }
  function renderItems() {
    return (
      data.map((item, index) =>
        renderItemsType(item, index)
      )
    )
  }

  return (
    <View style={localStyles.container}>
      <View style={localStyles.modalContainer}>
        <View style={{ padding: 16, paddingBottom: 0 }}>
          <Text style={localStyles.title}>{title}</Text>
        </View>
        {/* <View style={{ borderBottomWidth: 1, alignItems: 'center', padding: 16, borderColor: Constant.Color.THIN_GRAY }} /> */}
        {renderItems()}
      </View>
    </View>
  );
}


var localStyles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    width: Constant.Measure.SCREEN_WIDTH
  },
  modalContainer: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    width: Constant.Measure.SCREEN_WIDTH,
  },
  modal: {
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderColor: Constant.Color.THIN_GRAY,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 24,
    paddingHorizontal: 16,
    width: "100%",
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16
  }
})