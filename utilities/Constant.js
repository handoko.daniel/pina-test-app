import { Dimensions } from "react-native"
export default {
  Color: {
    PRIMARY: '#F16528',
    DARK_PRIMARY: '#497FBF',
    SECONDARY_PRIMARY: '#FFDC00',
    ABSOLUTE_WHITE: '#FFFFFF',
    THIN_GRAY: '#E1E1E1',
    THIN_GRAY_BACKGROUND: '#f1f1f1',
    DARK_GRAY: '#D8D8D8',
    ABSOLUTE_BLACK: '#111111',
    DARK_LABEL: '#292929',
    THIN_LABEL: '#9E9E9E',

    // utilities
    SUCCESS: '#7BCC29',
    WARNING: '#ffde8a',
    DANGER: '#F66158',
  },

  Measure: {
    SCREEN_WIDTH: Dimensions.get('screen').width,
    SCREEN_HEIGHT: Dimensions.get('screen').height
  }
}