// regex rules
// email should contain at least @ and . characters
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

// price will be separated with .
const currencySeparatorRegex = /(.)(?=(\d{3})+$)/g;

// normalize phonenumber
const normalizeStringRegex = /-/g;

class StringSanitizer {
    static normalizeText(text) {
        let normalizeText = text.toString().replace(normalizeStringRegex, "");
        return normalizeText;
    }

    static sanitizeText(text) {
        let sanitizedText = text.toString().replace(currencySeparatorRegex, '$1.');
        return sanitizedText;
    }

    static isValidEmail(text) {
        let isValid = emailRegex.test(text);
        return isValid;
    }

    static sanitizeTextValue(text) {
        if (text == "" || text == null) {
            return false;
        }
        else {
            return true;
        }
    }
}

export { currencySeparatorRegex };
export default StringSanitizer;